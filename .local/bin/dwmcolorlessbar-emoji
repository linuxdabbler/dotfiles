#!/bin/bash

speakericon="/usr/share/icons/Adwaita/16x16/devices/audio-speakers-symbolic.symbolic.png"
cpu() {
    read cpu a b c previdle rest < /proc/stat
	prevtotal=$((a+b+c+previdle))
	sleep 0.5
	read cpu a b c idle rest < /proc/stat
	total=$((a+b+c+idle))
	cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))

    cpu_value=$(grep -o "^[^ ]*" /proc/loadavg)

        echo "🖥️ $cpu_value%"
}

hdd() {
    free="$(df -h /home | awk '/dev/ {print $3}' | sed 's/G/Gb/')"
    perc="$(df -h /home | awk '/dev/ {print $5}')"
    number="$(df -h /home | awk '/dev/ {print $5}' | sed 's/%//')"
    
    if [[ $number -le 50 ]]; then
        echo "💾 $perc($free)"
    elif [[ $number -ge 51 ]]; then
        echo "💾 $perc($free)"
    elif [[ $number -ge 90 ]]; then
        echo "💾 $perc($free)"
    fi
    }

mem() {
    used="$(free | awk '/Mem:/ {print $3}')"
    total="$(free | awk '/Mem:/ {print $2}')"
    human="$(free -h | awk '/Mem:/ {print $3}' | sed 's/i//g')"
    percent="$(( 200 * $used/$total - 100 * $used/$total ))"
    ram="$(( 200 * $used/$total - 100 * $used/$total ))%($human) "

    if [[ $percent -le 25 ]]; then
        echo "🧠 $ram"
    elif [[ $percent -ge 26 ]]; then
        echo "🧠 $ram"
    elif [[ $percent -ge 76 ]]; then
        echo "🧠 $ram"
    fi
}

upgrades(){
	updates="$(aptitude search '~U' | wc -l)"
    if [ "$updates" = 0 ]; then
        printf "🧰 fully updated"
    else
    	printf "🧰 $updates updates"
    fi
}

networkicon() {
wire="$(ip a | awk '/enp/ && /inet/ || /eth/ && /inet/ {print $NF}' | wc -l)"
wifi="$(ip a | awk '/wlan/ && /inet/ {print $NF}' | wc -l)"
if [ $wire = 1 ]; then
    printf "🧵"
elif [ $wifi = 1 ]; then
    printf "🛜 "
else
    printf "🚫"
fi
}
#############################
#       IP ADDRESS
#############################
ipaddress() {
    address="$(ip a | awk '/.255/ {print $2}' | sed -e 's|/24||' -e 1q)"
    printf "$address"
}
############################
#       VPN CONNECTION
############################
vpnconnection() {
    state="$(ip a | awk '/tun0/ && /inet/ {print $NF}' | wc -l)"
if [ $state = 1 ]; then
    printf "🔐"
else
    printf "⚠️"
fi
}
volume() {
    VOLONOFF="$(amixer scontents | awk '/Left: Playback/ {print $NF}' | sed 's/[][]//g')"
    VOL="$(amixer scontents | awk '/Left: Playback/ {print $5}' | sed 's/[][]//g; s/%//')"
    LOWVOL="🔈 "
    MIDVOL="🔉"
    HIVOL="🔊 "
    VOLOFF="🔇"

    if [[ "$VOLONOFF" = "off" ]] || [[ "$VOL" = 0 ]]; then
        echo "$VOLOFF"
    elif [[ "$VOL" -ge 1 ]] && [[ "$VOL" -le 25 ]]; then
        echo "$LOWVOL $VOL%"
    elif [[ "$VOL" -ge 26 ]] && [[ "$VOL" -le 74 ]]; then
        echo "$MIDVOL $VOL%"
    elif [[ "$VOL" -ge 75 ]] ; then
        echo "$HIVOL $VOL%"
    fi
}

forecast() {
    weather=$(awk '/value/ {print $2" "$3}' ~/.config/weather.txt | sed -e 's/\"//g' -e 1q)
    case "$weather" in
        "Clear ") icon="☀️" ;;
        "Sunny ") icon="☀️" ;;
        "Partly cloudy") icon="⛅" ;;
        "Cloudy") icon="☁️ " ;;
        "Overcast") icon="☁️ " ;;
        "Fog") icon="☁️ " ;;
        "Mist") icon="☁️ " ;;
        "Light rain") icon=" 🌧️" ;;
        "Light drizzle") icon=" 🌧️" ;;
        "Rain") icon=" 🌧️ " ;;
        "Patchy rain") icon=" 🌧️"  ;;
        "Moderate rain") icon=" 🌧️" ;;
        "Thunderstorm in") icon="⛈️ " ;;
        *) icon="⛅" ;;
    esac

   if [ $icon = ☀️ ]; then
       echo "$icon $weather"
   else
       printf "$icon $weather"
   fi
}

temperature() {
temp=$(awk '/temp_F/ {print $2}' ~/.config/weather.txt | sed -e 's/\"//g' -e 's/,//')
tempicon="🌡️ "

if [ "$temp" -gt 80 ]; then
	printf  "$tempicon$temp F"
elif [ "$temp" -ge 50 ] && [ "$temp" -le 79 ]; then
	printf "$tempicon $temp F"
elif [ "$temp" -le 49 ]; then
	printf "$tempicon $temp F"
fi
}

clockinfo() {
    CLOCK=$(date '+%I')
    case "$CLOCK" in
    "00") icon="🕛" ;;
    "01") icon="🕐" ;;
    "02") icon="🕑" ;;
    "03") icon="🕒" ;;
    "04") icon="🕓" ;;
    "05") icon="🕔" ;;
    "06") icon="🕕" ;;
    "07") icon="🕖" ;;
    "08") icon="🕗" ;;
    "09") icon="🕘" ;;
    "10") icon="🕙" ;;
    "11") icon="🕚" ;;
    "12") icon="🕛" ;;
    esac
    echo "🗓️ $(date "+%b %d %Y (%a)") $(date "+$icon") $(date "+%R")"
}

while true; do

    xsetroot -name "$(cpu) | $(mem) | $(upgrades) | $(hdd) | $(networkicon) $(ipaddress) $(vpnconnection) | $(volume) | $(forecast) $(temperature) | $(clockinfo)"
    sleep 1
done
