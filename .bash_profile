# bash profile to be added to the home directory

# source ~/.profile
[ -e "$HOME/.profile" ] && . "$HOME/.profile"

#autostart dwm if logging in from TTY1
[[ "$tty" = "/dev/tty1" ]] && pgrep dwm || startx

# export ~/.local/bin to the PATH
export PATH="$PATH:$HOME/.local/bin"
